package trabalho1;

/**
 * Objetivo: Calcular e imprimir a tabuada de todas as operações (soma, subtração, multiplicação ou divisão)
 * de acordo com a opçao selecionada pelo usuário.
 * 
 * @author Leandro Silva do Nascimento
 */

import javax.swing.JOptionPane;

public class Tabuada {    
    public static void main(String[] args) {
        int opcao;
        do {
            //Menu de interação com usuário
            opcao = Integer.parseInt (JOptionPane.showInputDialog (null, "Seja Bem-Vindo. "+
            "\nEscolha a opção desejada"+
            "\n1 = Tabuada de Soma"+
            "\n2 = Tabuada de Subtração"+
            "\n3 = Tabuada de Divisão"+
            "\n4 = Tabuada de Multiplicação"+
            "\n0 = Sair"));
            int num1 = 1;
            int num2 = 1;
            //Switch case para executar a função escolhida
            switch (opcao){
                //Opção sair
                case 0:
                    JOptionPane.showMessageDialog (null, "Obrigado pela preferência. Volte sempre!.");
                    break;
                //Opção soma
                case 1: 
                    System.out.println ("Tabuada de Soma");
                    while (num1 <=9){
                        while (num2 <=9){
                            System.out.println (+num1+" + "+num2+" = "+(num1+num2));
                            num2++;
                        }
                        num2 = 1;
                        num1++;
                    }
                    break;
                //Opção subtração
                case 2:
                    System.out.println ("Tabuada de Subtração");
                    while (num1 <=9){
                        while (num2 <=9){
                            System.out.println (+num1+" - "+num2+" = "+(num1-num2));
                            num2++;
                        }
                        num2 = 1;
                        num1++;
                    }
                    break;
                //Opção divisão
                case 3:
                    System.out.println ("Tabuada de Divisão");
                    while (num1 <=9){
                        while (num2 <=9){
                            System.out.println (+num1+" / "+num2+" = "+(num1/num2));
                            num2++;
                        }
                        num2 = 1;
                        num1++;
                    }
                    break;
                //Opção multiplicação
                case 4:
                    System.out.println ("Tabuada de Multiplicação");
                    while (num1 <=9){
                        while (num2 <=9){
                            System.out.println (+num1+" * "+num2+" = "+(num1*num2));
                            num2++;
                        }
                        num2 = 1;
                        num1++;
                    }
                    break;
                //Caso não seja digitada uma opção de 0 a 4.
                default: 
                    JOptionPane.showMessageDialog (null, "Opção Inválida!! Tente novamente.");
                    break;
            }
        //Verificar se foi digitada uma opção diferente de 0, caso seja igual a 0 encerra o loop
        } while (opcao != 0);
    }
}